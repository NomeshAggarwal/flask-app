from flask import request, jsonify
from application.models import User
from application.routes import app, token_required


@app.route('/search/<username>')
@token_required
def search_user(username):
    userdata = User.query.filter_by(username=username).first()
    return jsonify({'Username': userdata.username, 'Email': userdata.email, 'City': userdata.city})
