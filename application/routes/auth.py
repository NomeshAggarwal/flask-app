from flask import request, jsonify
from application.routes import app
from application.models import User, db
from application import config
import jwt
import datetime
from werkzeug.security import generate_password_hash, check_password_hash


@app.route('/register', methods=['POST'])
def signup():
    formdata = request.get_json(force=True)
    hashed_password = generate_password_hash(formdata['password'], method='sha256')
    mydata = User(formdata['username'], formdata['city'], formdata['email'], hashed_password)
    try:
        db.session.add(mydata)
        db.session.commit()
        return jsonify({'message': 'Thank You for signing up'})
    except Exception as e:
        print(e)
        return jsonify({"message": "Oops! Something happened. User can't be created"})


@app.route('/login', methods=['POST'])
def login():
    formdata = request.get_json(force=True)
    mydata = User.query.filter_by(email=formdata['email']).first()
    if not mydata:
        return jsonify({'message': 'Invalid Login'})
    if check_password_hash(mydata.password, formdata['password']):
        token = jwt.encode(
            {'email': formdata['email'], 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30)},
            config.secret_key, algorithm="HS256")
        return jsonify({'token': token})
