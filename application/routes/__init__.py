from flask import Flask, request, jsonify
import jwt
from functools import wraps
from application import config

app = Flask('__name__')

from application.models import User


def token_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        authorization = request.headers.get("authorization")
        if not authorization:
            return jsonify({'message': 'Token is Missing'})
        try:
            bearer, auth_token = authorization.split()
            algorithm = "HS256"
            if not auth_token:
                return jsonify({'message': 'Token is Missing'})
            payload = jwt.decode(auth_token, config.secret_key, algorithms=[algorithm], verify=True)
        except Exception as e:
            print(e)
            return jsonify({'message': 'Token is Invalid'})
        auth_user = User.query.filter_by(email=payload['email']).first()
        if not auth_user:
            return jsonify({'message': 'User Not Found'})
        request.data = {"auth_user": auth_user}
        return f(*args, **kwargs)

    return decorated_function


from application.routes import auth, user
